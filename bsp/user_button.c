#include "user_button.h"
#include "stm32f4xx.h"                  // Device header

void init_user_button(void) {
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
    GPIOB->MODER &= ~GPIO_MODER_MODER15;
    GPIOB->OTYPER |= GPIO_OTYPER_OT_15;
    GPIOB->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR15;
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15_0;
    GPIOB->PUPDR &= GPIO_PUPDR_PUPDR15;
    GPIOB->PUPDR |= GPIO_PUPDR_PUPDR15_1;
}

uint8_t get_user_button(void) {
    return (GPIOB->IDR & (1<<15)) >> 15;
}

