#ifndef USER_BUTTON_H
#define USER_BUTTON_H
#include <stdint.h>

// User button is PB15

void init_user_button(void);
uint8_t get_user_button(void);

#endif /* USER_BUTTON_H */
