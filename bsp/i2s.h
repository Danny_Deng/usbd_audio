#ifndef I2S_H
#define I2S_H

#include "stm32f4xx.h"

#define AUDIO_I2S  SPI2


void init_i2s(void);

#endif /* I2S_H */
