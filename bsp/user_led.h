#ifndef USER_LED_H
#define USER_LED_H
#include "stm32f4xx.h"                  // Device header

// User LED is PC13

void init_user_led(void);
void user_led_on(void);
void user_led_off(void);
void user_led_toggle(void);

#endif /* USER_LED_H */
