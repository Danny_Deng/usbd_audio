#include "i2s.h"
#include "stm32f4xx.h"

/** I2S2 pin
 * CK: PB13
 * WS: PB12
 * SD: PB15
 */

void init_i2s(void) {
    /* Initialize Hardware */
    /* Initialize GPIO, mapping to I2S */
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
    RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;

    GPIOB->MODER |= GPIO_MODER_MODER12_1 | GPIO_MODER_MODER13_1 | GPIO_MODER_MODER15_1;
    GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR12 | GPIO_PUPDR_PUPDR13 | GPIO_PUPDR_PUPDR15);
    GPIOB->OTYPER &= ~(GPIO_OTYPER_IDR_12 | GPIO_OTYPER_IDR_13 | GPIO_OTYPER_IDR_15);
    GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12 | GPIO_OSPEEDER_OSPEEDR13 | GPIO_OSPEEDER_OSPEEDR15;
    /* remap */
    GPIOB->AFR[1] = (5 << 16) | (5<<20) | (5<<28);

    /* Initialize I2S */
    // Disable I2S
    AUDIO_I2S->I2SCFGR &= ~(SPI_I2SCFGR_I2SE);
    /** Setting I2S
     * I2S mode
     * Master transmit mode
     * MSB (left adjest standard)
     * 32 bits data length
     * 32 bits channal length
     */
    AUDIO_I2S->I2SCFGR = (SPI_I2SCFGR_I2SMOD | SPI_I2SCFGR_I2SCFG_1 | SPI_I2SCFGR_I2SSTD_0 | SPI_I2SCFGR_DATLEN_1 | SPI_I2SCFGR_CHLEN);
    AUDIO_I2S->I2SPR |= (SPI_I2SPR_MCKOE);
    // Enable I2S
    AUDIO_I2S->I2SCFGR |= (SPI_I2SCFGR_I2SE);


}
