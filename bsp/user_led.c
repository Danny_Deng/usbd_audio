#include "user_led.h"
#include "stm32f4xx.h"                  // Device header

void init_user_led(void) {
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
    GPIOC->MODER &= ~GPIO_MODER_MODER13;
    GPIOC->MODER |= GPIO_MODER_MODER13_0;
    GPIOC->OTYPER &= GPIO_OTYPER_OT_13;
    GPIOC->OSPEEDR &= ~GPIO_OSPEEDER_OSPEEDR13;
    GPIOC->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13_0;
    GPIOC->PUPDR &= GPIO_PUPDR_PUPDR13;
    user_led_off();
}

void user_led_on(void) {
   GPIOC->ODR &= ~GPIO_ODR_ODR_13;
}

void user_led_off(void) {
    GPIOC->ODR |= GPIO_ODR_ODR_13;
}

void user_led_toggle(void) {
    GPIOC->ODR ^= GPIO_ODR_ODR_13;
}

