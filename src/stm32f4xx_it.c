#include <stdbool.h>
#include "stm32f4xx_it.h"

void NMI_Handler(void) {
    while(true) {

    }
}


void HardFault_Handler(void) {
    /* Go to infinite loop when Hard Fault exception occurs */
    while (true) {

    }
}


void MemManage_Handler(void) {
    /* Go to infinite loop when Memory Manage exception occurs */
    while (true) {

    }
}


void BusFault_Handler(void) {
    /* Go to infinite loop when Bus Fault exception occurs */
    while (true) {

    }
}


void UsageFault_Handler(void) {
    /* Go to infinite loop when Usage Fault exception occurs */
    while (true) {

    }
}

void DebugMon_Handler(void) {
    while(true) {

    }
}

void NonMaskableInt_Handler(void) {
    while(true) {

    }
}
