#include "stm32f4xx.h"
#include "fpu.h"

#define VECT_TAB_OFFSET 0

static void SetSysClock(void);

void SystemInit(void) {
    /* Reset the RCC clock configuration to the default reset state ------------*/
    /* Set HSION bit */
    RCC->CR |= (uint32_t)0x00000001;

    /* Reset CFGR register */
    RCC->CFGR = 0x00000000;

    /* Reset HSEON, CSSON and PLLON bits */
    RCC->CR &= (uint32_t)0xFEF6FFFF;

    /* Reset PLLCFGR register */
    RCC->PLLCFGR = 0x24003010;

    /* Reset HSEBYP bit */
    RCC->CR &= (uint32_t)0xFFFBFFFF;

    /* Disable all interrupts */
    RCC->CIR = 0x00000000;

    /* Configure the System clock source, PLL Multiplier and Divider factors,
        AHB/APBx prescalers and Flash settings ----------------------------------*/
    SetSysClock();

    enable_fpu();

    /* Configure the Vector Table location add offset address ------------------*/
    SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH */

    /* Enable FPU */
    SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));  /* set CP10 and CP11 Full Access */
}

static void SetSysClock(void) {
    /* Initialzie PLL for USB full speed */
    // clock = 16 MHz
    // F_pll = clock * (N / M) / (P)
    RCC->CR |= RCC_CR_PLLON;
/**
 * Q: 4
 * P: 0
 * N: 192
 * M: 16
 * PLL: 48 MHZ
 */
    RCC->AHB2ENR |= RCC_AHB2ENR_OTGFSEN;

    /* Initialzie PLLI2S */
    // F_plli2s = clock * (N / M) / (R)
    RCC->CR |= RCC_CR_PLLI2SON;
/**
 * R: 2
 * N: 3
 * M: 16
 * PLL: 1.5 MHZ
 */
    RCC->PLLI2SCFGR = (2<<28) | (3<<6);


}

