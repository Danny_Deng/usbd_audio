#include <stdio.h>
#include <stdbool.h >

// Include FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

// Include USER LED and Button
#include "user_led.h"
#include "user_button.h"

// Include BSP
#include "stm32f4xx.h"
#include "i2s.h"
#include "fs_usb.h"

void vApplicationTickHook(void) {

}

void vApplicationIdleHook(void) {

}

void ulMainGetRunTimeCounterValue(void) {

}

void vApplicationStackOverflowHook(void) {

}

void vMainConfigureTimerForRunTimeStats(void) {

}

volatile uint32_t global_cnt1 = 0;
void task1(__attribute__((unused)) void *pvParameters) {
	while(true){
        global_cnt1++;
        AUDIO_I2S->DR = 0xAAAA;
        vTaskDelay(1);
        taskYIELD();
	}
}
void task2(__attribute__((unused)) void *pvParameters) {
	while(true){
        if(get_user_button()) {
            user_led_on();
        }
        else {
            user_led_off();
        }
        taskYIELD();
	}
}

int main (void) {
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    /* Initialize hardward */
    init_user_led();
    init_user_button();
    init_i2s();
    init_usbd();

    xTaskCreate(task1,
                "task1",
                100,
                NULL,
                1,
                NULL
                );
    xTaskCreate(task2,
                "task2",
                100,
                NULL,
                1,
                NULL
                );

    __enable_irq();
    vTaskStartScheduler();

    while (true) {
        ;
    }

    return 0;
}

